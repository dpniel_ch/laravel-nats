<?php

namespace LaravelNats;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use LaravelNats\Events\NatsBinaryEvent;
use LaravelNats\Events\NatsJsonEvent;
use LaravelNats\Events\NatsPhpEvent;
use LaravelNats\Events\NatsRawEvent;
use LaravelNats\Nats\Connection;
use LaravelNats\Nats\ConnectionOptions;
use LaravelNats\Nats\EncodedConnection;
use LaravelNats\Nats\Encoders\IGBinaryEncoder;
use LaravelNats\Nats\Encoders\JSONEncoder;
use LaravelNats\Nats\Encoders\PHPEncoder;
use LaravelNats\Nats\Encoders\YAMLEncoder;
use LaravelNats\Pool\Pool;

class LaravelNats
{
    // Build your next great package.
    private $encoding;
    private $client = null;
    private $options = null;
    private $pool;

    private $event_map = [
        "raw" => NatsRawEvent::class,
        "json" => NatsJsonEvent::class,
        "php" => NatsPhpEvent::class,
        "binary" => NatsBinaryEvent::class
    ];

    public function __construct(?string $encoding = null, ?int $timeout = null)
    {
        $this->encoding = $encoding;
        if (!is_null($encoding)) {
            $this->options = new ConnectionOptions(config("nats.connection", null));
            $this->client = $this->createClient();
            $this->client->connect($timeout ?? config("nats.timeout", 240));
        }
//        $this->pool = Pool::create()->timeout(5);
    }

//    public function pool() : Pool
//    {
//        return $this->pool;
//    }

    public static function raw(?int $timeout = null): LaravelNats
    {
        return new self("raw");
    }

    public static function json(?int $timeout = null): LaravelNats
    {
        return new self("json");
    }

    public static function php(?int $timeout = null): LaravelNats
    {
        return new self("php");
    }

    public static function binary(?int $timeout = null): LaravelNats
    {
        return new self("binary");
    }

    public function emit($topic, $payload)
    {
        event(new $this->event_map[$this->encoding](static::removePrefix($topic, $this->encoding), $payload));
    }

    public function publish($topic, $payload)
    {
        $this->client->publish(static::addPrefix($topic, $this->encoding), $payload);
    }

//    public function publishAsync($topic, $payload)
//    {
//        $prefixed_topic = static::addPrefix($topic, $this->encoding);
//        $config = config("nats", null);
//        $encoder = null;
//        if ($this->encoding !== "raw") {
//            $encoder = $this->getEncoder();
//        }
//        $this->pool->add(static function () use ($prefixed_topic, $payload, $encoder, $config) {
//            $options = new ConnectionOptions($config["connection"]);
//            $client = null;
//            if (is_null($encoder)) {
//                $client = new Connection($options);
//            } else {
//                $client = new EncodedConnection($options, new $encoder());
//            }
//            $client->connect($config["timeout"]);
//
//            $client->publish($prefixed_topic, $payload);
//            return null;
//        })->then(static function ($reply) use (&$result) {
//            $result = $reply;
//        })->timeout(static function () use ($topic, $payload) {
//            Log::error("NATS::request() timedout: " . $topic . " : " . print_r($payload, true));
//        });
//    }

    public function request($topic, $payload, $callback = null)
    {
        $result = null;
        if (is_null($callback)) {
            $callback = static function ($reply) use (&$result) {
                $result = $reply->getBody();
            };
        }
        $this->client->request(static::addPrefix($topic, $this->encoding), $payload, $callback);
        return $result;
    }

//    public function requestAsync($topic, $payload)
//    {
//        $prefixed_topic = static::addPrefix($topic, $this->encoding);
//        $config = config("nats", null);
//        $result = null;
//        $encoder = null;
//        if ($this->encoding !== "raw") {
//            $encoder = $this->getEncoder();
//        }
//        $this->pool->add(static function () use ($prefixed_topic, $payload, $encoder, $config) {
//            $options = new ConnectionOptions($config["connection"]);
//            $client = null;
//            if (is_null($encoder)) {
//                $client = new Connection($options);
//            } else {
//                $client = new EncodedConnection($options, new $encoder());
//            }
//            $client->connect($config["timeout"]);
//
//            $result = null;
//            $client->request($prefixed_topic, $payload, function ($reply) use (&$result) {
//                $result = $reply->getBody();
//            });
//            return $result;
//        })->then(static function ($reply) use (&$result) {
//            $result = $reply;
//        })->timeout(static function () use ($topic, $payload) {
//            Log::error("NATS::request() timedout: " . $topic . " : " . print_r($payload, true));
//        });
//        $this->pool->wait();
//        return $result;
//    }

    public function subscribe($topic, $callback)
    {
        return $this->client->subscribe(static::addPrefix($topic, $this->encoding), $callback);
    }

    public function queueSubscribe($topic, $queue, $callback)
    {
        return $this->client->queueSubscribe(static::addPrefix($topic, $this->encoding), static::addPrefix($queue, $this->encoding), $callback);
    }

    public function wait($number = 0)
    {
        return $this->client->wait($number);
    }

//    public function waitAsync()
//    {
//        return $this->pool->wait();
//    }

    public function close()
    {
        $this->client->close();
    }

    public static function addPrefix($topic, $encoding)
    {
        return config("nats.prefix") . ".{$encoding}.{$topic}";
    }

    public static function removePrefix($topic, $encoding)
    {
        return Str::after($topic, config("nats.prefix") . ".{$encoding}.");
    }

    private function createClient()
    {
        switch ($this->encoding) {
            case "raw":
                return new Connection($this->options);
            default:
                return new EncodedConnection($this->options, $this->getEncoder());
        }
    }

    private function getEncoder()
    {
        switch ($this->encoding) {
            case "json":
                return  new JSONEncoder();
            case "php":
                return new PHPEncoder();
            case "yaml":
                return new YAMLEncoder();
            case "binary":
                return new IGBinaryEncoder();
            default:
                throw new \RuntimeException("Invalid encoding format");
        }
    }
}
