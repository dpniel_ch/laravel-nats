<?php


namespace LaravelNats\Events;


use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NatsSubscriptionEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $topic;
    public $message;

    public function __construct(string $topic, $message)
    {
        $this->topic = $topic;
        $this->message = $message;
    }
}
