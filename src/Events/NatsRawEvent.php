<?php


namespace LaravelNats\Events;


use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NatsRawEvent extends NatsSubscriptionEvent
{
}
