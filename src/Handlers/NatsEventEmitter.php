<?php

namespace LaravelNats\Handlers;

class NatsEventEmitter implements NatsHandlerInterface
{

    public function handle(&$client, &$message)
    {
        $client->emit($message->getSubject(), $message->getBody());
    }
}
