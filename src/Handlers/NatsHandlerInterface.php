<?php

namespace LaravelNats\Handlers;

interface NatsHandlerInterface
{
    public function handle(&$connection, &$message);
}
