<?php


namespace LaravelNats\Handlers;


class NatsEchoHandler implements NatsHandlerInterface
{
    // All this does is echos back the received body
    public function handle(&$connection, &$message)
    {
        $message->reply($message->getBody());
    }
}
