<?php

namespace LaravelNats;

use LaravelNats\Console\ListenCommand;
use Illuminate\Support\ServiceProvider;
use LaravelNats\Console\QuitCommand;
use LaravelNats\Console\RestartCommand;

class LaravelNatsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('nats.php'),
            ], 'config');

            // Registering package commands.
             $this->commands([
                 ListenCommand::class,
                 QuitCommand::class,
                 RestartCommand::class
             ]);
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'nats');

        // Register the main class to use with the facade
        $this->app->singleton('nats', function () {
            return new LaravelNats;
        });
    }
}
