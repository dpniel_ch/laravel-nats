<?php


namespace LaravelNats\Pool;


use LaravelNats\Pool\Exceptions\SerializableException;
use LaravelNats\Pool\Process\ParallelProcess;

class PoolStatus
{
    /**
     * @var Pool
     */
    private $pool;

    /**
	 * PoolStatus constructor.
	 * @param Pool $param
	 */
	public function __construct(Pool $param)
	{
	    $this->pool = $param;
	}

    public function __toString(): string
    {
        return $this->lines(
            $this->summaryToString(),
            $this->failedToString()
        );
    }

    protected function lines(string ...$lines): string
    {
        return implode(PHP_EOL, $lines);
    }

    protected function summaryToString(): string
    {
        return
            'pending: '.$this->pool->pending()->count()
            .' - inProgress: '.$this->pool->inProgress()->count()
            .' - finished: '.$this->pool->finished()->count()
            .' - failed: '.$this->pool->failed()->count()
            .' - timeout: '.$this->pool->timeouts()->count();
    }

    protected function failedToString(): string
    {
        return $this->pool->failed()->reduce(function ($result, ParallelProcess $process) {
            $output = $process->getErrorOutput();
            if ($output instanceof SerializableException) {
                $output = get_class($output->asThrowable()) . ": " . $output->asThrowable()->getMessage();
            }
            return $this->lines($result, "{$process->getPid()} failed with {$output}");
        }, "");
    }
}
