<?php


namespace LaravelNats\Pool;


use InvalidArgumentException;
use Illuminate\Support\Collection;
use LaravelNats\Pool\Process\ParallelProcess;
use LaravelNats\Pool\Process\Runnable;
use LaravelNats\Pool\Runtime\ParentRuntime;

class Pool
{
    protected $concurrency = 20;
    protected $tasksPerProcess = 1;
    // Set timeout to 0 to disable
    protected $timeout = 300;
    protected $sleepTime = 50000;
    protected $queues;
    protected $results;
    protected $status;
    protected $stopped = false;

    public function __construct()
    {
        $this->registerListener();
        $this->queues = Collection::make([
            "pending" => new Collection(),
            "inProgress" => new Collection(),
            "finished" => new Collection(),
            "failed" => new Collection(),
            "timeouts" => new Collection()
        ]);
        $this->results = new Collection();
        $this->status = new PoolStatus($this);
    }

    public static function create()
    {
        return new static();
    }

    public function add($process, ?int $outputLength = null): Runnable
    {
        if (!is_callable($process) && !$process instanceof Runnable) {
            throw new InvalidArgumentException("The process passed to Pool::add should be callable");
        }

        if (! $process instanceof Runnable) {
            $process = ParentRuntime::createProcess(
                $process,
                $outputLength,
                PHP_BINARY
            );
        }

        $this->putInQueue($process);

        return $process;
    }

    public function wait(?callable $intermediateCallback = null): Collection
    {
        while ($this->queues["inProgress"]->isNotEmpty()) {
            $this->queues["inProgress"]->each(function (Runnable $process, $pid){
                if ($this->timeout === 0) {
                    return;
                }
                if ($process->getCurrentExecutionTime() > $this->timeout) {
                    $this->markAsTimedOut($process);
                }
            });

            if ($this->queues["inProgress"]->isEmpty()) {
                break;
            }

            if ($intermediateCallback) {
                $intermediateCallback($this);
            }

            usleep($this->sleepTime);
        }

        return $this->results;
    }

    public function autoload(string $autoloader): Pool
    {
        ParentRuntime::init($autoloader);
        return $this;
    }

    public function concurrency(int $concurrency): Pool
    {
        $this->concurrency = $concurrency;
        return $this;
    }

    public function timeout(int $timeout): Pool
    {
        $this->timeout = $timeout;
        return $this;
    }

    public function sleepTime(int $sleepTime): Pool
    {
        $this->sleepTime = $sleepTime;
        return $this;
    }

    public function pending() : Collection
    {
        return $this->queues->get("pending");
    }

    public function inProgress() : Collection
    {
        return $this->queues->get("inProgress");
    }

    public function finished() : Collection
    {
        return $this->queues->get("finished");
    }

    public function failed() : Collection
    {
        return $this->queues->get("failed");
    }

    public function timeouts() : Collection
    {
        return $this->queues->get("timeouts");
    }

    public function status(): PoolStatus
    {
        return $this->status;
    }

    public function stop(): void
    {
        $this->stopped = true;
    }

    public function notify(): void
    {
        if ($this->inProgress()->count() >= $this->concurrency) {
            return;
        }

        if ($this->pending()->isEmpty()) {
            return;
        }

        $this->putInProgress($this->pending()->shift());
    }

    public function putInQueue(Runnable $process): void
    {
        $this->queues["pending"]->put($process->getPid(), $process);
        $this->notify();
    }

    public function putInProgress(Runnable $process): void
    {
        if ($this->stopped) {
            return;
        }

        if ($process instanceof ParallelProcess) {
            $process->getProcess()->setTimeout($this->timeout);
        }
        $process->start();

        $this->queues["pending"]->forget($process->getPid());
        $this->queues["inProgress"]->put($process->getPid(), $process);
    }

    public function markAsFinished(Runnable $process): void
    {
        $this->queues["inProgress"]->forget($process->getPid());
        $this->notify();
        $this->results->push($process->triggerSuccess());
        $this->queues["finished"]->put($process->getPid(), $process);
    }

    public function markAsTimedOut(Runnable $process): void
    {
        $this->queues["inProgress"]->forget($process->getPid());
        $process->stop();
        $process->triggerTimeout();
        $this->queues["timeouts"]->put($process->getPid(), $process);
        $this->notify();
    }

    public function markAsFailed(Runnable $process): void
    {
        $this->queues["inProgress"]->forget($process->getPid());
        $this->notify();
        $process->triggerError();
        $this->queues["failed"]->put($process->getPid(), $process);
    }

    protected function registerListener()
    {
        pcntl_async_signals(true);
        pcntl_signal(SIGCHLD, function ($sig, $status) {
            while (true) {
                $pid = pcntl_waitpid(-1, $processState, WNOHANG | WUNTRACED);
                if ($pid <= 0) {
                    break;
                }
                if (!$this->queues["inProgress"]->has($pid)) {
                    continue;
                }
                $process = $this->queues["inProgress"]->get($pid);
                if ($status["status"] === 0) {
                    $this->markAsFinished($process);
                    continue;
                }
                $this->markAsFailed($process);
            }
        });
    }
}
