<?php


namespace LaravelNats\Pool\Runtime;


use Closure;
use LaravelNats\Pool\Process\ParallelProcess;
use LaravelNats\Pool\Process\Runnable;
use Opis\Closure\SerializableClosure;
use Symfony\Component\Process\Process;

class ParentRuntime
{
    protected static $isInitialised = false;

    protected static $autoloader;
    protected static $bootloader;

    protected static $childProcessScript;

    protected static $currentId = 0;

    protected static $myPid = null;

    public static function init(string $autoloader = null): void
    {
        if (! $autoloader) {
            $existingAutoloaderFiles = collect([
                __DIR__.'/../../../../../autoload.php',
                __DIR__.'/../../../../autoload.php',
                __DIR__.'/../../../autoload.php',
                __DIR__.'/../../vendor/autoload.php',
                __DIR__.'/../../../vendor/autoload.php',
                __DIR__.'/../../../../vendor/autoload.php',
            ])->filter(static function (string $path) {
                return file_exists($path);
            });
            $autoloader = $existingAutoloaderFiles->first();
        }

        self::$autoloader = $autoloader;

        $existingAutoloaderFiles = collect([
            __DIR__.'/../../../../../bootstrap/app.php',
            __DIR__.'/../../../../bootstrap/app.php',
            __DIR__.'/../../../bootstrap/app.php',
            __DIR__.'/../../bootstrap/app.php',
        ])->filter(static function (string $path) {
            return file_exists($path);
        });
        self::$bootloader = $existingAutoloaderFiles->first();

        self::$autoloader = $autoloader;
        self::$childProcessScript = __DIR__.'/ChildRuntime.php';

        self::$isInitialised = true;
    }

    public static function createProcess($task, ?int $outputLength = null, ?string $binary = 'php'): Runnable
    {
        if (! self::$isInitialised) {
            self::init();
        }

        $process = new Process([
            $binary,
            self::$childProcessScript,
            self::$autoloader,
            self::encodeTask($task),
            self::$bootloader
        ]);

        return ParallelProcess::create($process, self::getId());
    }


    public static function encodeTask($task): string
    {
        if ($task instanceof Closure) {
            $task = new SerializableClosure($task);
        }

        return base64_encode(serialize($task));
    }

    public static function decodeTask(string $task)
    {
        return unserialize(base64_decode($task));
    }

    protected static function getId(): string
    {
        if (self::$myPid === null) {
            self::$myPid = getmypid();
        }

        ++self::$currentId;

        return (string) self::$currentId.(string) self::$myPid;
    }
}
