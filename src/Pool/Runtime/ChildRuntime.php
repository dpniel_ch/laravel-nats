<?php

use Illuminate\Auth\SessionGuard;
use LaravelNats\Pool\Runtime\ParentRuntime;

try {
    $autoloader = $argv[1] ?? null;
    $serializedClosure = $argv[2] ?? null;
    $bootloader = $argv[3] ?? null;
    $outputLength = (1024 * 10);

    if (! $autoloader) {
        throw new InvalidArgumentException('No autoloader provided in child process.');
    }

    if (! file_exists($autoloader)) {
        throw new InvalidArgumentException("Could not find autoloader in child process: {$autoloader}");
    }

    if (! $serializedClosure) {
        throw new InvalidArgumentException('No valid closure was passed to the child process.');
    }

    require $autoloader;
    $app = null;
    $kernel = null;
    $isLaravel = true;
    if (file_exists($bootloader)) {
        $app = require_once $bootloader;
        if (0 === strpos($app->version(), "Lumen")) {
            $isLaravel = false;
        }
        if (!$app) {
            throw new RuntimeException("Laravel bootstrap file not found");
        }
        $kernel = $app->make($isLaravel ? 'Illuminate\Contracts\Http\Kernel'  : 'Laravel\Lumen\Application');
    }


    $task = ParentRuntime::decodeTask($serializedClosure);

    $output = call_user_func($task);

    $serializedOutput = base64_encode(serialize($output));

    if (strlen($serializedOutput) > $outputLength) {
        throw LaravelNats\Pool\Exceptions\ParallelError::outputTooLarge($outputLength);
    }

    fwrite(STDOUT, $serializedOutput);

    if (!is_null($app)) {
        $app->terminate($input, $status);
    }

    exit(0);
} catch (Throwable $exception) {
    require_once __DIR__.'/../Exceptions/SerializableException.php';

    $output = new \LaravelNats\Pool\Exceptions\SerializableException($exception);

    fwrite(STDERR, base64_encode(serialize($output)));

    exit(1);
}
