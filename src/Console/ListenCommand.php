<?php


namespace LaravelNats\Console;


use Carbon\Carbon;
use LaravelNats\Handlers\NatsEventEmitter;
use LaravelNats\LaravelNats;
use Illuminate\Console\Command;

class ListenCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nats:listen {--encoding=json : encoding to subscribe topics to} { --queued : Use queue_subscriptions }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start a nats listener';

    /**
     * Indicates whether the command should be shown in the Artisan command list.
     *
     * @var bool
     */
    protected $hidden = false;

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $encoding = $this->option("encoding");

        $client = LaravelNats::{$encoding}();

        // For the php artisan nats:quit command
        $client->subscribe("quit", function ($data) use ($client) {
            $this->info(Carbon::now()->toDateTimeString() . "Quit received. Closing connection ....");
            $client->close();
        });

        // For the php artisan nats:restart command
        $client->subscribe("restart", function ($data) use ($client) {
            $this->info(Carbon::now()->toDateTimeString() . "Restart received....");
            $client->close();
            $this->handle();
        });

        if ($this->option("queued")) {
            $subscriptions = collect(config("nats.queue_subscriptions.{$encoding}"));
            if ($subscriptions->isEmpty()) {
                return $this->info(Carbon::now()->toDateTimeString() . " No queue subscriptions to listen on");
            }

            // Subscribe to configured topics
            $subscriptions->each(function ($config, $sub) use ($client) {
                $client->queueSubscribe($sub, $config["queue"], function ($data) use ($client, $config) {
                    $this->info(Carbon::now()->toDateTimeString() . " <-- [nats] RCV " . $data->getSubject());
                    if (is_null($config["handler"])) {
                        (new NatsEventEmitter())->handle($client, $data);
                    } else {
                        (new $config["handler"])->handle($client, $data);
                    }
                    $this->info("Memory Size: " . (memory_get_usage() /1024/1024));
                });
            });
        } else {
            $subscriptions = collect(config("nats.subscriptions.{$encoding}"));
            if ($subscriptions->isEmpty()) {
                return $this->info(Carbon::now()->toDateTimeString() . " No subscriptions to listen on");
            }

            // Subscribe to configured topics
            $subscriptions->each(function ($handler, $sub) use ($client) {
                $client->subscribe($sub, function ($data) use ($client, $handler) {
                    $this->info(Carbon::now()->toDateTimeString() . " <-- [nats] RCV " . $data->getSubject());
                    if (is_null($handler)) {
                        (new NatsEventEmitter())->handle($client, $data);
                    } else {
                        (new $handler)->handle($client, $data);
                    }
                    $this->info("Memory Size: " . (memory_get_usage() /1024/1024));
                });
            });
        }

        $this->info(Carbon::now()->toDateTimeString() . 'Nats started successfully.');
        $client->wait();
    }
}
