<?php


namespace LaravelNats\Console;


use Carbon\Carbon;
use LaravelNats\Events\NatsSubscriptionEvent;
use LaravelNats\LaravelNats;
use Illuminate\Console\Command;

class QuitCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nats:quit {--encoding=json : encoding to quit listeners for}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Quit a nats listener';

    /**
     * Indicates whether the command should be shown in the Artisan command list.
     *
     * @var bool
     */
    protected $hidden = false;

    /**
     * Execute the console command.
     *
     * @param LaravelNats $client
     * @return void
     */
    public function handle()
    {
        $client = LaravelNats::{$this->option("encoding")}();
        $client->publish("quit", []);
    }
}
