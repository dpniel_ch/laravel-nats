<?php


namespace LaravelNats\Console;


use Carbon\Carbon;
use LaravelNats\Events\NatsSubscriptionEvent;
use LaravelNats\LaravelNats;
use Illuminate\Console\Command;

class RestartCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nats:restart {--encoding=json : encoding to restart listeners for}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restart a nats listener';

    /**
     * Indicates whether the command should be shown in the Artisan command list.
     *
     * @var bool
     */
    protected $hidden = true;

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $client = LaravelNats::{$this->option("encoding")}();
        $client->publish("quit", []);
    }
}
