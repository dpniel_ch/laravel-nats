<?php

namespace LaravelNats\Tests;

use Illuminate\Support\Collection;
use LaravelNats\LaravelNats;
use Orchestra\Testbench\TestCase;
use LaravelNats\LaravelNatsServiceProvider;

class PublishSubscribeTest extends TestCase
{

    protected function getPackageProviders($app)
    {
        return [LaravelNatsServiceProvider::class];
    }

    protected function setUp(): void
    {
        parent::setUp();
        config(["nats.prefix" => "testing"]);
    }

    public function test_pub_sub_resp_raw()
    {
        $client = LaravelNats::raw();
        $client->subscribe("foo", function ($data) {
            $this->assertEquals("bar", $data);
        });
        $client->publish("foo", "bar");
        $client->wait(1);
    }

//    public function test_async_pub_sub_resp_raw()
//    {
//        $client = LaravelNats::raw();
//        $client->subscribe("foo", function ($data) {
//            $this->assertEquals("bar", $data);
//        });
//        sleep(1);
//        $client->publishAsync("foo", "bar");
//        $client->wait(1);
//    }

    public function test_pub_sub_resp_json()
    {
        $client = LaravelNats::json();
        $client->subscribe("foo", function ($data) {
            $this->assertEquals("bar", $data->getBody()[0]);
        });
        $client->publish("foo", ["bar"]);
        $client->wait(1);
    }

//    public function test_async_pub_sub_resp_json()
//    {
//        $client = LaravelNats::json();
//        $client->subscribe("foo", function ($data) {
//            $this->assertEquals("bar", $data->getBody()[0]);
//        });
//        sleep(1);
//        $client->publishAsync("foo", ["bar"]);
//        $client->wait(1);
//    }

    public function test_pub_sub_resp_php()
    {
        $client = LaravelNats::php();
        $client->subscribe("foo", function ($data) {
            $this->assertInstanceOf(Collection::class, $data->getBody());
            $this->assertTrue($data->getBody()->contains("test"));
        });
        $client->publish("foo", new Collection(["test", "data"]));
        $client->wait(1);
    }

//    public function test_async_pub_sub_resp_php()
//    {
//        $client = LaravelNats::php();
//        $client->subscribe("foo", function ($data) {
//            $this->assertInstanceOf(Collection::class, $data->getBody());
//            $this->assertTrue($data->getBody()->contains("test"));
//        });
//        sleep(1);
//        $client->publishAsync("foo", new Collection(["test", "data"]));
//        $client->wait(1);
//    }

    public function test_pub_sub_resp_igbinary()
    {
        $client = LaravelNats::binary();
        $client->subscribe("foo", function ($data) {
            $this->assertInstanceOf(Collection::class, $data->getBody());
            $this->assertTrue($data->getBody()->contains("test"));
        });
        $client->publish("foo", new Collection(["test", "data"]));
        $client->wait(1);
    }

//    public function test_async_pub_sub_resp_igbinary()
//    {
//        $client = LaravelNats::binary();
//        $client->subscribe("foo", function ($data) {
//            $this->assertInstanceOf(Collection::class, $data->getBody());
//            $this->assertTrue($data->getBody()->contains("test"));
//        });
//        $client->publishAsync("foo", new Collection(["test", "data"]));
//        $client->wait(1);
//    }
}
