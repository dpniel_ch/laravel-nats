<?php


namespace LaravelNats\Tests\Pool;


use LaravelNats\LaravelNatsServiceProvider;
use Opis\Closure\SerializableClosure;
use Orchestra\Testbench\TestCase;
use LaravelNats\Pool\Pool;
use Symfony\Component\Process\Process;
use Symfony\Component\Stopwatch\Stopwatch;

class PoolTest extends TestCase
{
    protected $stopwatch;

    protected function getPackageProviders($app)
    {
        return [LaravelNatsServiceProvider::class];
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->stopwatch = new StopWatch();
    }

    public function test_it_can_run_processes_in_parallel()
    {
        $pool = Pool::create();

        $this->stopwatch->start('test');

        foreach (range(1, 5) as $i) {
            $pool->add(function () {
                usleep(1000);
            });
        }
        $pool->wait();

        $stopwatchResult = $this->stopwatch->stop('test');

        $this->assertLessThan(400, $stopwatchResult->getDuration(), "Execution time was {$stopwatchResult->getDuration()}, expected less than 400.\n".(string) $pool->status());
    }

    public function test_it_can_handle_success()
    {
        $pool = Pool::create();

        $counter = 0;

        foreach (range(1, 5) as $i) {
            $pool->add(function () {
                return 2;
            })->then(function (int $output) use (&$counter) {
                $counter += $output;
            });
        }

        $pool->wait();

        $this->assertEquals(10, $counter, (string) $pool->status());
    }

    public function test_it_can_handle_timeout()
    {
        $pool = Pool::create()
            ->timeout(1);

        $counter = 0;

        foreach (range(1, 5) as $i) {
            $pool->add(function () {
                sleep(2);
            })->timeout(function () use (&$counter) {
                $counter += 1;
            });
        }

        $pool->wait();

        $this->assertEquals(5, $counter, (string) $pool->status());
    }

    public function test_it_can_handle_a_maximum_of_concurrent_processes()
    {
        $pool = Pool::create()
            ->concurrency(2);

        $startTime = microtime(true);

        foreach (range(1, 3) as $i) {
            $pool->add(function () {
                sleep(1);
            });
        }

        $pool->wait();

        $endTime = microtime(true);

        $executionTime = $endTime - $startTime;

        self::assertGreaterThanOrEqual(2, $executionTime, "Execution time was {$executionTime}, expected more than 2.\n".(string) $pool->status());
        self::assertEquals(3, $pool->finished()->count(), (string) $pool->status());
    }

    public function test_it_can_use_a_class_from_the_parent_process()
    {
        $pool = Pool::create();
        $result = null;

        $pool->add(static function () {
            $class = new MyParentClass();

            $class->property = true;

            return $class;
        })->then(static function (MyParentClass $class) use (&$result) {
            $result = $class;
        });

        $pool->wait();

        self::assertInstanceOf(MyParentClass::class, $result);
        self::assertTrue($result->property);
    }

    public function test_it_can_return_all_the_output_as_an_array()
    {
        $pool = Pool::create();

        $result = null;

        foreach (range(1, 5) as $i) {
            $pool->add(static function () {
                return 2;
            });
        }

        $result = $pool->wait();

        self::assertCount(5, $result);
        self::assertEquals(10, array_sum($result->toArray()));
    }

    public function test_it_can_take_an_intermediate_callback()
    {
        $pool = Pool::create();

        $pool->add(static function () {
            return 1;
        });

        $isIntermediateCallbackCalled = false;

        $pool->wait(static function (Pool $pool) use (&$isIntermediateCallbackCalled) {
            $isIntermediateCallbackCalled = true;
        });

        self::assertTrue($isIntermediateCallbackCalled);
    }

    public function test_it_can_be_stopped_early()
    {
        $concurrency = 20;
        $stoppingPoint = $concurrency / 5;

        $pool = Pool::create()->concurrency($concurrency);

        $maxProcesses = 10000;
        $completedProcessesCount = 0;

        for ($i = 0; $i < $maxProcesses; $i++) {
            $pool->add(static function () use ($i) {
                return $i;
            })->then(static function ($output) use ($pool, &$completedProcessesCount, $stoppingPoint) {
                $completedProcessesCount++;

                if ($output === $stoppingPoint) {
                    $pool->stop();
                }
            });
        }

        $pool->wait();

        /**
         * Because we are stopping the pool early (during the first set of processes created), we expect
         * the number of completed processes to be less than 2 times the defined concurrency.
         */
        self::assertGreaterThanOrEqual($stoppingPoint, $completedProcessesCount);
        self::assertLessThanOrEqual($concurrency * 2, $completedProcessesCount);
    }
}
