<?php


namespace LaravelNats\Tests\Pool;


use LaravelNats\LaravelNatsServiceProvider;
use Opis\Closure\SerializableClosure;
use Orchestra\Testbench\TestCase;
use Symfony\Component\Process\Process;

class ChildRuntimeTest extends TestCase
{
    protected $child = "childworker";

    protected function getPackageProviders($app)
    {
        return [LaravelNatsServiceProvider::class];
    }

    public function test_it_can_run_child_runtime(): void
    {
        $bootstrap = __DIR__ . '/../../src/Pool/Runtime/ChildRuntime.php';

        $autoloader = __DIR__ . '/../../vendor/autoload.php';

        $child_string = $this->child;

        $serializedClosure = base64_encode(serialize(new SerializableClosure(function () use ($child_string) {
            echo $child_string;
        })));

        $process = new Process([
            'php',
            $bootstrap,
            $autoloader,
            $serializedClosure,
            null,
        ]);

        $process->start();

        $process->wait();
//        dd(base64_decode($process->getErrorOutput()));
        self::assertStringContainsString('childworker', $process->getOutput());
    }

}
