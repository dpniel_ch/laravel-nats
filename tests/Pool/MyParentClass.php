<?php


namespace LaravelNats\Tests\Pool;


class MyParentClass
{
    public $property = null;

    public function throwException()
    {
        throw new MyParentException('test');
    }
}
