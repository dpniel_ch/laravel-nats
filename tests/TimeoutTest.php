<?php

namespace LaravelNats\Tests;

use Illuminate\Support\Collection;
use LaravelNats\LaravelNats;
use Orchestra\Testbench\TestCase;
use LaravelNats\LaravelNatsServiceProvider;

class TimeoutTest extends TestCase
{

    protected function getPackageProviders($app)
    {
        return [LaravelNatsServiceProvider::class];
    }

    protected function setUp(): void
    {
        parent::setUp();
        config(["nats.prefix" => "testing"]);
    }

    public function test_connect_timeout()
    {
        $client = LaravelNats::raw(5);
        $client->subscribe("foo", function ($data) {
            $this->assertEquals("bar", $data);
            $data->reply($data->getBody() . " baz");
        });
        $client->request("foo", "bar", function ($reply) {
            $this->assertEquals("bar baz", $reply->getBody());
        });
//        $client->publish("foo", "bar");
        $client->wait(1);
    }

}
