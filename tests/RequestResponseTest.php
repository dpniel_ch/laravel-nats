<?php

namespace LaravelNats\Tests;

use Illuminate\Support\Collection;
use LaravelNats\LaravelNats;
use Orchestra\Testbench\TestCase;
use LaravelNats\LaravelNatsServiceProvider;

class RequestResponseTest extends TestCase
{

    protected function getPackageProviders($app)
    {
        return [LaravelNatsServiceProvider::class];
    }

    protected function setUp(): void
    {
        parent::setUp();
        config(["nats.prefix" => "testing"]);
    }

    public function test_req_resp_raw()
    {
        $client = LaravelNats::raw();
        $client->subscribe("foo", function ($data) {
            $this->assertEquals("bar", $data);
            $data->reply($data->getBody() . " baz");
        });
        $client->request("foo", "bar", function ($reply) {
            $this->assertEquals("bar baz", $reply->getBody());
        });
    }

    public function test_req_resp_json()
    {
        $client = LaravelNats::json();
        $client->subscribe("foo", function ($data) {
            $this->assertEquals("bar", $data->getBody()[0]);
            $data->reply($data->getBody()[] = "baz");
        });
        $client->request("foo", ["bar"], function ($reply) {
            $this->assertEquals("bar", $reply->getBody()[0]);
            $this->assertEquals("baz", $reply->getBody()[1]);
        });
    }

    public function test_req_resp_php()
    {
        $client = LaravelNats::php();
        $client->subscribe("foo", function ($data) {
            $this->assertInstanceOf(Collection::class, $data->getBody());
            $collection = $data->getBody();
            $collection->push("bar");
            $collection->push("baz");
            $data->reply($collection);
        });
        $client->request("foo", new Collection(["test", "data"]), function ($reply) {
            $this->assertEquals(4, $reply->getBody()->count());
            $this->assertTrue($reply->getBody()->contains("baz"));
        });
    }

    public function test_req_resp_igbinary()
    {
        $client = LaravelNats::binary();
        $client->subscribe("foo", function ($data) {
            $this->assertInstanceOf(Collection::class, $data->getBody());
            $collection = $data->getBody();
            $collection->push("bar");
            $collection->push("baz");
            $data->reply($collection);
        });
        $client->request("foo", new Collection(["test", "data"]), function ($reply) {
            $this->assertEquals(4, $reply->getBody()->count());
            $this->assertTrue($reply->getBody()->contains("baz"));
        });
    }
}
