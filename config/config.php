<?php

use LaravelNats\Handlers\NatsEchoHandler;
use LaravelNats\Handlers\NatsEventEmitter;

return [
    "timeout" => 240,
    "prefix" => env("NATS_PREFIX", env("APP_ENV")),
    "connection" => [
        "host" => env("NATS_HOST", "localhost"),
        "port" => env("NATS_PORT", 4222),
        "user" => env("NATS_USER", null),
        "pass" => env("NATS_PASS", null),
        "token" => env("NATS_TOKEN", null)
    ],
    /**
     * Topic subscriptions can be seperated by their desired encoding
     */
    "subscriptions" => [
        // Raw string passthrough
        "raw" => [],
        // json encoded
        "json" => [
            // null defaults to NatsEventEmitter handler
            // Which in this case would emit the NatsJsonEvent
            "foo" => null,
            // Or you can implement your own handler that implements
            // the NatsHandlerInterface
            "echo" => NatsEchoHandler::class
        ],
        // php serialize()'d
        "php" => [],
        // IGBianry Serialised
        "binary" => []
    ],
    /**
     * Queued subscriptions enable concurrency in that we can create multiple listeners
     * on the same topic but it will only get delivered to one listener
     */
    "queue_subscriptions" => [
        // Raw string passthrough
        "raw" => [],
        // json encoded
        "json" => [
            // null defaults to NatsEventEmitter handler
            // Which in this case would emit the NatsJsonEvent
            "foo" => [
                "queue" => "foo_queue",
                "handler" => null
            ],
            // Or you can implement your own handler that implements
            // the NatsHandlerInterface
            "echo" => [
                "queue" => "echo_queue",
                "handler" => NatsEchoHandler::class
            ]
        ],
        // php serialize()'d
        "php" => [],
        // IGBianry Serialised
        "binary" => []
    ]
];
